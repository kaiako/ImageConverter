import edu.duke.*;

import java.awt.*;
import java.io.File;


/**
 * Created by kaiak on 18.02.2017.
 */
public class GrayScaleConverter {


    public ImageResource makeGray(ImageResource inImage){
    ImageResource outImage = new ImageResource(inImage.getWidth(),inImage.getHeight());
        for(Pixel pixel: outImage.pixels()) {

          Pixel inPixel = inImage.getPixel(pixel.getX(), pixel.getY());

            int average = (inPixel.getRed() + inPixel.getBlue() + inPixel.getGreen())/3;
            pixel.setRed(average);
            pixel.setGreen(average);
            pixel.setBlue(average);

        }return outImage;
    }

    public void testGray() {
        ImageResource ir = new ImageResource();
        ImageResource gray = makeGray(ir);
        gray.draw();
    }

public void selectAndConvert() {
DirectoryResource dr = new DirectoryResource();
for(File f : dr.selectedFiles()) {
    ImageResource inImage = new ImageResource(f);
    ImageResource gray = makeGray(inImage);
    String fName = inImage.getFileName();
    String newName = fName + "-gray";
    gray.setFileName(newName);

    gray.save();

    gray.draw();
}

}

}
